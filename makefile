#makefile for remar

CPP = g++ -g
#this is for debugging
NVCa = g++ -g
NVC = nvcc -g -diag-suppress 940
#supress warnings about returns from operator overloads


LOP = -lIex -lIlmImf -lIlmThread -lImath -lIex -lHalf
LDP = -Bstatic -lIex -lIlmImf -lIlmThread -lImath -lIex -lHalf -Bdynamic
#this is for debugging
NVCFa = 
NVCFFa = 
NVCF = --x cu -gencode arch=compute_61,code=sm_61
NVCFF = -gencode arch=compute_61,code=sm_61

OBJ = scene.o camera.o sample.o ray.o output.o de_object.o vec3.o quart.o io.o surface.o frame.o
HDF = source/scene.h source/camera.h source/sample.h source/ray.h source/output.h source/de_object.h source/vec3.h source/quart.h source/io.h source/surface.h source/pixel.h source/const.h source/frame.h 
OUT = remar



.PHONY: all clean

all: main

clean:
	rm main.o ${OBJ} ${OUT}

main: main.o ${OBJ}
	${NVC} ${NVCFF} main.o ${OBJ} ${LOP} -o ${OUT} 

main.o: source/main.cpp ${HDF}
	${CPP} -c source/main.cpp -o main.o

scene.o: source/scene.cpp ${HDF}
	${CPP} -c source/scene.cpp -o scene.o

#this is for debugging (change between camera.cu and camera.cpp)
camera.o: source/camera.cu ${HDF}
	${NVC} ${NVCF} -dc -c source/camera.cu -o camera.o

sample.o: source/sample.cpp ${HDF}
	${NVC} ${NVCF} -dc -c source/sample.cpp -o sample.o

ray.o: source/ray.cpp ${HDF}
	${NVC} ${NVCF} -dc -c source/ray.cpp -o ray.o 


#this is where one can choose different DE implementations
# de_object.o: source/de_object_spheres.cpp source/de_object.h source/vec3.h source/quart.h source/surface.h
# 	${NVC} ${NVCF} -dc -c source/de_object_spheres.cpp -o de_object.o
de_object.o: source/de_object_sierpinski.cpp source/de_object.h source/vec3.h source/quart.h source/surface.h
	${NVC} ${NVCF} -dc -c source/de_object_sierpinski.cpp -o de_object.o


vec3.o: source/vec3.cpp source/vec3.h source/quart.h
	${NVC} ${NVCF} -dc -c source/vec3.cpp -o vec3.o

quart.o: source/quart.cpp source/quart.h
	${NVC} ${NVCF} -dc -c source/quart.cpp -o quart.o

output.o: source/output.cpp source/output.h source/pixel.h
	${CPP} -c source/output.cpp -o output.o

io.o: source/io.cpp
	${CPP} -c source/io.cpp -o io.o

surface.o: source/surface.cpp source/vec3.h source/quart.h
	${NVC} ${NVCF} -dc -c source/surface.cpp -o surface.o

frame.o: source/frame.cpp source/frame.h source/vec3.h
	${NVC} ${NVCF} -dc -c source/frame.cpp -o frame.o
