# Remar

Fully Pathmarched (as in Pathtracing but with Raymarching) Renderer.

Remar is a project to develop an efficient, but also powerfull rendering engine, that uses raymarching while also calculating reflections, indirect lighting etc. following the same principle as a pathtracer. Furthermore, Remar should also include support for features needed in VFX work, such as import/export of camera data, multiple passes in the output file etc.
