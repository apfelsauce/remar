#ifndef _PIXEL
#define _PIXEL

#include <iostream>

#include "vec3.h"

//struct for pixels
struct pixel{
    //rgb-colour values as vec3
    vec3 diff_col;
    vec3 diff_lig;
    vec3 glos_col;
    vec3 glos_lig;
    vec3 tran_col;
    vec3 tran_lig;
    //alpha-value
    float alpha;
    //normal of object as vec3
    vec3 normal;
    //distance to object
    float distance;
    int rayType;
};

#endif
