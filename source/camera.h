#ifndef _CAMERA
#define _CAMERA

#ifndef __device__
#define __device__
#endif

#ifndef __host__
#define __host__
#endif

#ifndef __global__
#define __global__
#endif

#include <iostream>
#include <vector>

#include "vec3.h"
#include "pixel.h"
#include "de_object.h"

//these aren't used yet, but they should be later
#include "sample.h"
#include "ray.h"



//using namespace std;

class camera{
    std::vector<vec3> directions;
    std::vector<pixel> image;
public:
    vec3 dir;
    vec3 pos;
    vec3 up;
    
    float fLen;
    int resX;
    int resY;
    
    int samples;
    int bounces = 15;
    
    de_object DE;
    
    camera(){};
    
    camera(float fLen, int px_X, int px_Y, de_object DEin,int samples,vec3 position,vec3 direction, vec3 upDir);
    
    void move(vec3 position, vec3 direction);
    
    //
    //This method calculates all the needed ray-directions to start the render
    //
    //This particular implementation uses squared-samples.
    //
    //
    void calculate_directions();
    
    __host__
    std::vector<pixel> * render(std::vector<pixel> *rImg);
    
    
    

};

__global__
//void renderPix(int n, pixel* Pix, vec3 pos, vec3 *directions, int bounces, de_object DE, int *li);
void renderPix(int n, pixel* Pix, vec3 pos, vec3 *directions, int bounces, de_object DE);

#endif
