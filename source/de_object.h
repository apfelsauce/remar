#ifndef _DE_OBJECT
#define _DE_OBJECT

#ifndef __device__
#define __device__
#endif

#ifndef __host__
#define __host__
#endif

#include <iostream>
#include <cmath>
#include <vector>

#include "vec3.h"
#include "quart.h"
#include "surface.h"


#ifndef PI
#define PI 3.14159265
#endif

struct de{
    double distance;
    
    
    vec3 norm;
    
    vec3 repos;
    
    surfer surf;
};

//using namespace std;
class de_object{
public:
    __device__ __host__
    de_object(){
    };
    
    vec3 norm;
    
    __device__ __host__
    de DE(vec3 pos);
    
    void update(std::vector<double> params);
};

#endif
