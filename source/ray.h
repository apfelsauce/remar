#ifndef _RAY
#define _RAY

#ifndef __device__
#define __device__
#endif

#ifndef __host__
#define __host__
#endif

#include <iostream>

#include "const.h"

#include "vec3.h"
#include "pixel.h"
#include "de_object.h"


class ray{
    public:
    vec3 pos;
    vec3 dir;
    vec3 norm;
    de_object DE;
    de distO;

    __device__ __host__
    ray(vec3 position, vec3 direction, de_object DEin);
    
    __device__ __host__
    int march_step();
    
    __device__ __host__
    bool march_to_end();
};

#endif
