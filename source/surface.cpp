#include "surface.h"

__device__ __host__
surfer::surfer(vec3 dif, vec3 gls, vec3 trs, float rgh, float indexOfRefraction){
    this->diff_col = dif;
    this->glos_col = gls;
    this->tran_col = trs;
    this->roughness = rgh;
    this->ior = indexOfRefraction;
}


/*different ray-types:
 *  0: Diffuse
 *  1: Glossy
 *  2: Transmission
 */
__device__ __host__
int surfer::surfType(int ran){
    //determine absolut values of different 
    double ga = glos_col.abs();
    double da = diff_col.abs();
    double ta = tran_col.abs();
    if(ga+ta+da==0)
        return 0;
    //if statements are inefficient
//     if(ta==0){
//         if(ga==0)
//             return 0;
//         if(da==0)
//             return 1;
//         
//         double f =(double) (ran % 10)/9;
//         double br = da/(da+ga);
//         if(f<br)
//             return 0;
//         return 1;
//     }else if(ga==0)){
//         if(ta==0)
//             return 0;
//         if(da==0)
//             return 2;
//         
//         double f =(double) (ran % 10)/9;
//         double br = da/(da+ta);
//         if(f<br)
//             return 0;
//         return 2;
//     }else if(da==0)){
//         if(ga==0)
//             return 1;
//         if(ta==0)
//             return 2;
//         
//         double f =(double) (ran % 10)/9;
//         double br = ga/(ga+ta);
//         if(f<br)
//             return 1;
//         return 2;
//     }
    
    //random float
    double f =(double) (ran % 233)/232;
    //glossy-threshhold
    double gt = da/(ga+ta+da);
    //transmission-threshhold
    double tt = (da+ga)/(da+ta+ga);
    //return ray-type dependent on random-float and threshholds
    return (int) (f>=tt) + (f>=gt);
    
}
