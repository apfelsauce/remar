#ifndef _QUART
#define _QUART

#ifndef __device__
#define __device__
#endif

#ifndef __host__
#define __host__
#endif

#include <iostream>
#include <cstdlib>
#include <cmath>

#ifndef PI
#define PI 3.14159265
#endif

using namespace std;

//defining quarternions

class quart{
public:
    double e;
    double i;
    double j;
    double k;
    
    __device__ __host__
    quart(){}
    
    
    __device__ __host__
    quart(double e, double i, double j, double k);
    
    
    
    //------------------operator overloads---------------------//        
        __device__ __host__
        quart operator+(const quart& v);
        
        __device__ __host__
        quart operator-(const quart& v);
        
        __device__ __host__
        quart operator*(const quart& v);
        
        //two operator overloads for scaling with ints(one alone doesn't seem to do the trick.)
        __device__ __host__
        quart operator*(const int& f);
        
        //Note: friends don't seem to get along outside of class-defs.
        __device__ __host__
        friend quart &operator*(const int& f, const quart &v){
            quart q(0,0,0,0);
            q.e = v.e * f;
            q.i = v.i * f;
            q.j = v.j * f;
            q.k = v.k * f;
        }
        
        //two operator overloads for scaling with doubles(one alone doesn't seem to do the trick.)
        __device__ __host__
        quart operator*(const double& f);
        
        __device__ __host__
        friend quart &operator*(const double& f, const quart &v){
            quart q(0,0,0,0);
            q.e = v.e * f;
            q.i = v.i * f;
            q.j = v.j * f;
            q.k = v.k * f;
        }

        //output operator overload
        friend ostream &operator<<(ostream &output, const quart &v){
            output << "1:"<<v.e<< " i:"<<v.i<< " j:"<<v.j<< " k:"<<v.k;
            return output;
        }
            
        //---------------end of operator overloads------------------//
        
        //#########################################################//
        //--------this is the beginning of normal methods----------//
        
        //absolute value
        __device__ __host__
        double abs();
        
        //normalized version of the vector
        __device__ __host__
        quart normalize();
        
        //return conjugated
        __device__ __host__
        quart conjugate();
};

#endif
