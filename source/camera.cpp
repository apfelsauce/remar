#include "camera.h"


// #define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
// inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
// {
//    if (code != cudaSuccess) 
//    {
//       fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
//       if (abort) exit(code);
//    }
// }


camera::camera(float fLen, int px_X, int px_Y, de_object DEin,int samples,vec3 position,vec3 direction, vec3 upDir){
    this->fLen = fLen;
    this->resY = px_Y;
    this->resX = px_X;
    this->DE = DEin;
    this->samples = samples;
    this->dir=direction;
    this->pos=position;
    this->up =upDir;
}

void camera::move(vec3 position, vec3 direction){
    this->dir=direction;
    this->pos=position;
}


//
//This method calculates all the needed ray-directions to start the render
//
//This particular implementation uses squared-samples.
//
//
void camera::calculate_directions(){
    //horizontal FOV in radians
    double foVX = 2*atan(18/this->fLen);
    //vertical FOV -> not (really) needed, unless non-square pixels are desired
    double foVY = foVX/resX*resY;
    
    //now calculate the angle between rays (determined by pixel-count and sample-per-pixels)
    //this assumes squared samples
    double rayAngle = foVX/resX/samples;
    
    double xStart = foVX / 2;
    double yStart = foVY / 2;
    
    //to-do: determine the needed rotation-axis for the camera!
    vec3 xDir;//rotation-axis to rotate x around (side to side)
    vec3 yDir;//rotation-axis to rotate y around (up-down)
    
    //the yDir is the cross-product of the camera-direction and up (thus it's sideways)
    yDir = this->dir.cross(this->up);
    //the xDir is the cross-product of the camera-direction and the yDir, it's the camera's local up.
    xDir = yDir.cross(this->dir);
    
    
    
    //for every vertical line
    for(int yPix = 0; yPix < this->resY; yPix++){
        //and every pixel in that line
        for(int xPix = 0; xPix<this->resX;xPix++){
            //Now do all the samples
            //this ensures that all the values for each pixel are next to each other in the result
            for(int ySample = 0; ySample<this->samples; ySample++){
                for(int xSample = 0; xSample<this->samples; xSample++){
                    //calculate the actual x-y "position" of the sample:
                    int x = xPix*samples+xSample;
                    int y = yPix*samples+ySample;
                    double xAngle = xStart-rayAngle*x;
                    double yAngle = yStart-rayAngle*y;
                    
                    vec3 newDir = this->dir;
                    
                    
                    //to-do: rotate newDir by the given angles around certain axis:
                    newDir = newDir.rotate(yDir,yAngle+2*PI);
                    newDir = newDir.rotate(xDir,xAngle+2*PI);
                    
                    this->directions.push_back(newDir.normalize());
                }
            }

        }
        
    }
}

__host__
std::vector<pixel>*  camera::render(std::vector<pixel> *rImg){
    /*initiate rays for each pixel(*sample) */
    /*render all these rays (on the GPU)*/
    
    int N = resY*resX*samples*samples;
    
    //first calculate all the directions of the individual samples
    this->calculate_directions();
    //and get an array pointer to them
    //vec3 *dirs = &(this->directions)[0];
    cout<<"calculated directions"<<endl;
    
//     cout<<"b"<<endl;
    pixel *image;
    image = (pixel*) malloc(N*sizeof(pixel));
//     cout<<"c"<<endl;
    for (int i =0; i<N;i++){
        sample Smp(this->pos, this->directions[i], this->bounces,this->DE);
//         cout<<"b"<<endl;
        image[i] = Smp.render_sample(5);
    }
    
    cout<<"saving..."<<endl;
    
    //sample-count (this works for square-samples)
    int sam = samples*samples;
    //cast c++ array to std::vector (and make sure all the samples for each pixel are averaged)
    (*rImg).clear();
    //for every pixel
    for(int i = 0; i<resY*resX; i++){
//         std::move(image[i])
        //cout<<li[i]<<endl;
        //make clean pixel;
        pixel pix;
        pix.alpha = 0;
        pix.diff_col = vec3(0,0,0);
        pix.diff_lig = vec3(0,0,0);
        pix.glos_col = vec3(0,0,0);
        pix.glos_lig = vec3(0,0,0);
        pix.distance = 0;
        
        //for every sample (they are all stored one after the other (see camera::calculate_directions))  -- add them all up
        for(int e = 0; e<sam; e++){
            pix.alpha += image[i*sam+e].alpha;
            pix.diff_col = pix.diff_col + image[i*sam+e].diff_col;
            pix.diff_lig = pix.diff_lig + image[i*sam+e].diff_lig;
            pix.glos_col = pix.glos_col + image[i*sam+e].glos_col;
            pix.glos_lig = pix.glos_lig + image[i*sam+e].glos_lig;
            pix.distance += image[i*sam+e].distance;
        }
        //average
        pix.alpha = pix.alpha / sam;
        pix.diff_col = pix.diff_col * ((double) 1/sam);
        pix.diff_lig = pix.diff_lig * ((double) 1/sam);
        pix.glos_col = pix.glos_col * ((double) 1/sam);
        pix.glos_lig = pix.glos_lig * ((double) 1/sam);
        pix.distance = pix.distance / sam;
        
        (*rImg).push_back(pix);
    }
    //free memory:
//     cudaFree(image);
//     cudaFree(li);
    /*return them*/
    return rImg;
}

/*
 * this is the function handed to each gpu-"thread"
 */
// __global__
// void renderPix(int n, pixel* Pix, vec3 pos, vec3 *directions, int bounces, de_object DE, int *li){
// //void renderPix(int n, pixel* Pix, vec3 pos, vec3 *directions, int bounces, de_object DE){
//     int index = blockIdx.x * blockDim.x + threadIdx.x;
//     int stride = blockDim.x * gridDim.x;
//     for (int i = index; i < n; i+=stride){
//         li[i]=5;
//         sample sam(pos, directions[i], bounces,DE);
//         li[i]=6;
//         Pix[i]=sam.render_sample();
//         li[i]=7;        
//     }
// }


