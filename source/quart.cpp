 #include "quart.h"

__device__ __host__
quart::quart(double e, double i, double j, double k){
    this->e = e;
    this->i = i;
    this->j = j;
    this->k = k;
}

// __device__ __host__
// quart::quart(vec3 v, double k){
//     this->e = v.x;
//     this->i = v.y;
//     this->j = v.z;
//     this->k = k;
// }

//------------------operator overloads---------------------//        
__device__ __host__
quart quart::operator+(const quart& v){
    quart q(0,0,0,0);
    q.e = this->e + v.e;
    q.i = this->i + v.i;
    q.j = this->j + v.j;
    q.k = this->k + v.k;
    return q;
}

__device__ __host__
quart quart::operator-(const quart& v){
    quart q(0,0,0,0);
    q.e = this->e - v.e;
    q.i = this->i - v.i;
    q.j = this->j - v.j;
    q.k = this->k - v.k;
    return q;
}

__device__ __host__
quart quart::operator*(const quart& v){
    quart q(0,0,0,0);
    q.e = (this->e * v.e) - (this->i * v.i) - (this->j * v.j) - (this->k * v.k);
    q.i = (this->i * v.e) + (this->e * v.i) + (this->j * v.k) - (this->k * v.j);
    q.j = (this->j * v.e) + (this->e * v.j) + (this->k * v.i) - (this->i * v.k);
    q.k = (this->k * v.e) + (this->e * v.k) + (this->i * v.j) - (this->j * v.i);
    return q;
}

//two operator overloads for scaling with ints(one alone doesn't seem to do the trick.)
__device__ __host__
quart quart::operator*(const int& f){
    quart q(0,0,0,0);
    q.e = this->e * f;
    q.i = this->i * f;
    q.j = this->j * f;
    q.k = this->k * f;
    return q;
}



//two operator overloads for scaling with doubles(one alone doesn't seem to do the trick.)
__device__ __host__
quart quart::operator*(const double& f){
    quart q(0,0,0,0);
    q.e = this->e * f;
    q.i = this->i * f;
    q.j = this->j * f;
    q.k = this->k * f;
    return q;
}



//---------------end of operator overloads------------------//

//#########################################################//
//--------this is the beginning of normal methods----------//



double quart::abs(){
    return sqrt(this->e*this->e + this->i*this->i + this->j*this->j + this->k*this->k);
}


quart quart::normalize(){
    quart q;
    double asv = this->abs();
    
    q.e = this->e/asv;
    q.i = this->i/asv;
    q.j = this->j/asv;
    q.k = this->k/asv;
    
    return q;
}


__device__ __host__
quart quart::conjugate(){
    quart q;
    q.e =  this->e;
    q.i = -this->i;
    q.j = -this->j;
    q.k = -this->k;
    return q;
}



