#ifndef _SAMPLE
#define _SAMPLE

#ifndef __device__
#define __device__
#endif

#ifndef __host__
#define __host__
#endif

#include <iostream>

#include "const.h"

#include "vec3.h"
#include "pixel.h"
#include "de_object.h"

#include "output.h"

//these aren't used yet, but they should be later
#include "ray.h"

//using namespace std;

//
//several helpfull functions, previously located in sample.cpp?
//

//create a simple procedural sky
__device__ __host__
vec3 sky(vec3 V);

//generate random doubles
__device__ __host__
double randd(int seed);


//generate random vec3s
__device__ __host__
vec3 randv(int seed);


//double-mod function
__device__ __host__
double dmod2(double n, int div);


//--------------------------------------------------------------//
//      the sample class which takes over the job of creating and
//      evaluating rays
//--------------------------------------------------------------//

class sample{
    vec3 dir;
    vec3 pos;
    de_object DE;
public:
    int bounces;
    //maybe these should go somewhere else (they might have to be tuned.)
    //Distance to definite hit
    const double MIN_DIST = 0.0001;
    //If run out of steps, this is the minimum distance to hit.
    const double MIN_DIST2 = 0.01;
    //the maximum amount of steps to take for one ray
    const int MAX_STEPS = 128;
    
    //constructor
    __device__ __host__
    sample(vec3 position, vec3 direction, int bounces,de_object DEin);
    
    //creates ray & executes it
    __device__ __host__
    pixel render_sample(int index);
    
    //resets the ray for the next bounces
    __device__ __host__
    void resetRay(ray Ray, int rayType, int bounce, int seed);
};

#endif
