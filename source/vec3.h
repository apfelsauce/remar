#ifndef _VEC3
#define _VEC3

#ifndef __device__
#define __device__
#endif

#ifndef __host__
#define __host__
#endif

#include <iostream>
#include <cstdlib>
#include <cmath>

#include "quart.h"

#include "const.h"


using namespace std;

//defining 3d vectors.

class vec3{
    public:
        double x;
        double y;
        double z;
        
        __device__ __host__
        vec3(){
            
        }
        
        __device__ __host__
        vec3(double x, double y, double z);
        
        
        //------------------operator overloads---------------------//        
        __device__ __host__
        vec3 operator+(const vec3& v);
        
        __device__ __host__
        vec3 operator-(const vec3& v);
        
        //two operator overloads for scaling with ints(one alone doesn't seem to do the trick.)
        __device__ __host__
        vec3 operator*(const int& i);
        
        //Note: friends don't seem to get along outside of class-defs.
        __device__ __host__
        friend vec3 &operator*(const int& i, const vec3 &v){
            vec3 vec(0,0,0);
            vec.x = v.x * i;
            vec.y = v.y * i;
            vec.z = v.z * i;
        }
        
        //two operator overloads for scaling with doubles(one alone doesn't seem to do the trick.)
        __device__ __host__
        vec3 operator*(const double& i);
        
        __device__ __host__
        friend vec3 &operator*(const double& i, const vec3 &v){
            vec3 vec(0,0,0);
            vec.x = v.x * i;
            vec.y = v.y * i;
            vec.z = v.z * i;
        }

        //output operator overload
        friend ostream &operator<<(ostream &output, const vec3 &v){
            output << "x:"<<v.x<< " y:"<<v.y<< " z:"<<v.z;
            return output;
        }
            
        //---------------end of operator overloads------------------//
        
        
        //#########################################################//
        //--------this is the beginning of normal methods----------//
        
        //dot-product:
        __device__ __host__
        double dot(vec3 v);
        
        //cross-product:
        __device__ __host__
        vec3 cross(vec3 v);
        
        //pointwise scale/multiplication:
        __device__ __host__
        vec3 vScale(vec3 v);
        
        //absolute value (length)
        __device__ __host__
        double abs();
        
        //positive values for x,y and z:
        __device__ __host__
        vec3 pos();
        
        //normalized version of the vector
        __device__ __host__
        vec3 normalize();
        
        
        //Stuff for rotation, axis-specific could be removed?
        
        //shady rotation matrix buisness going on in here!
        __device__ __host__
        vec3 rotateX(double angle);
        
        __device__ __host__
        vec3 rotateY(double angle);
        
        __device__ __host__
        vec3 rotateZ(double angle);
        
        //even worse stuff, straight from wikipedia!
        __device__ __host__
        vec3 rotate(vec3 u, double angle/*in radians*/);
        
        //get spherical coordinates
        //x==radius, y==theta, z==phi
        __device__ __host__
        vec3 sphC();
        
};

#endif
