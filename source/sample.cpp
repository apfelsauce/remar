#include "sample.h"


//ok, no idea, what these values mean, i don't think they are used anywhere?
#define znew (z=36969*(z&65535)+(z>>16))
#define wnew (w=18000*(w&65535)+(w>>16))
#define MWC ((znew<<16)+wnew )
#define SHR3 (jsr^=(jsr<<17), jsr^=(jsr>>13), jsr^=(jsr<<5))
#define CONG (jcong=69069*jcong+1234567)
#define FIB ((b=a+b),(a=b-a))
#define KISS ((MWC^CONG)+SHR3)




__device__ __host__
sample::sample(vec3 position, vec3 direction, int bounces, de_object DEin){
    this->bounces = bounces;
    this->pos = position;
    this->dir = direction.normalize();
    this->DE = DEin;
}


//--------------------------------------------------------------//
//      This is the main-render function for the sample
//--------------------------------------------------------------//
__device__ __host__
pixel sample::render_sample(int seed){
    pixel Pixel;
    //constant ray-type
    int rayType = 0;
    //initialize randomness
    
    //code to render stuff
    int bounce = 0;
    Pixel.diff_lig=Pixel.glos_lig=Pixel.tran_lig=vec3(1,1,1);
    Pixel.diff_col=Pixel.glos_col=Pixel.tran_col=vec3(1,1,1);
    
    //line to test out random stuff:
//     Pixel.diff_col = Pixel.glos_col = Pixel.tran_col=vec3((float) (seed%31)/31,(float) (seed%233)/233,(float) (seed%173)/173);
    
    Pixel.distance = 0;
    Pixel.alpha = 1;
    
    
/////////////////////////////////////////////////////////////////////////////
//      the first bounce is handled seperately
/////////////////////////////////////////////////////////////////////////////
    
    ray Ray(this->pos,this->dir, this->DE);
    bool hit=Ray.march_to_end();
    if(! hit){
        Pixel.distance = INFT;
        Pixel.alpha = 0;
        // Pixel.glos_col=Pixel.diff_col=Pixel.tran_col=vec3(1,1,1);
        Pixel.diff_lig=Pixel.glos_lig=Pixel.tran_lig=sky(this->dir);
        return Pixel;
    }
    Pixel.distance = (this->pos - Ray.pos).abs();
    
    //color the pixel by reading in the surface colors
    Pixel.diff_col = Ray.distO.surf.diff_col;
    Pixel.glos_col = Ray.distO.surf.glos_col;
    Pixel.tran_col = Ray.distO.surf.tran_col;
    // Pixel.alpha = 1; //<- pixel alpha should already be 1
    //determine the ray-type (and store for pixel)
    rayType = Ray.distO.surf.surfType(seed);
    Pixel.rayType = rayType;
    //and set the other light to "off"
    Pixel.diff_lig = Pixel.diff_lig*(rayType==0);
    Pixel.glos_lig = Pixel.glos_lig*(rayType==1);
    Pixel.tran_lig = Pixel.tran_lig*(rayType==2);
    
    //debugging code: turn the pixels into colors to check ray-types
//     if(rayType == 0){
//         Pixel.glos_col=Pixel.diff_col=Pixel.tran_col=vec3(1,0,0);
//     }else if(rayType == 1){
//         Pixel.glos_col=Pixel.diff_col=Pixel.tran_col=vec3(0,1,0);
//     }else if(rayType == 2){
//         Pixel.glos_col=Pixel.diff_col=Pixel.tran_col=vec3(0,0,1);
//     }
//     return Pixel;
    //prepare to compute the next bounce
    
    
/////////////////////////////////////////////////////////////////////////////
//      do all the other bounces
/////////////////////////////////////////////////////////////////////////////
    for(bounce = 1; bounce < this->bounces; bounce++){
        
        //prepare the ray
        resetRay(Ray,rayType, bounce-1, seed);
        //initialize Ray
        ray Ray(this->pos,this->dir, this->DE);
        //march it
        bool hit=Ray.march_to_end();
        
        //if we didn't hit, then put in some sky
        //this is a pointwise multiplication via the vScale (=vector Scale) function of vec3
        if(! hit){
                vec3 skai = sky(Ray.dir);
                Pixel.diff_lig = Pixel.diff_lig.vScale(sky(Ray.dir));
                Pixel.glos_lig = Pixel.glos_lig.vScale(sky(Ray.dir));
                Pixel.tran_lig = Pixel.tran_lig.vScale(sky(Ray.dir));
            return Pixel;
        }
        
        //TODO else color the light-value appropiately (ok, this is not yet propper)
        //this is a pointwise multiplication via the vScale function of vec3
        Pixel.diff_lig = Pixel.diff_lig.vScale((Ray.distO.surf.diff_col + Ray.distO.surf.glos_col + Ray.distO.surf.tran_col)*0.33);
        Pixel.glos_lig = Pixel.glos_lig.vScale(Ray.distO.surf.diff_col + Ray.distO.surf.glos_col + Ray.distO.surf.tran_col)*0.33;
        Pixel.tran_lig = Pixel.tran_lig.vScale(Ray.distO.surf.diff_col + Ray.distO.surf.glos_col + Ray.distO.surf.tran_col)*0.33;
        
    }
    
    //debugging stuff
//     Pixel.rgb = vec3(0.5,0.6,0.7);
//     Pixel.diff_col=Pixel.glos_col=vec3(seed,seed,seed);
//     Pixel.alpha = 1;
//     cout<<"I'm a sample"<<endl;
    // Pixel.alpha = 1; //<- pixel alpha should already be 1
    return Pixel;
}


//--------------------------------------------------------------//
//      reset to the old ray's endpoint and determine the new direction
//      outsourced, because it needs to be done for the first && each subsequent bounce
//--------------------------------------------------------------//

void sample::resetRay(ray Ray, int rayType, int bounce, int seed){
    //reset the ray to start the next sample
    this->pos = Ray.pos;
    
    //get a semi-random type for the next step
    //TODO why does the function take a rayType and then calculate a new one?
    rayType = Ray.distO.surf.surfType((seed+(bounce * 271))%241*7+seed);
    
    
    int sig;
    double tnew;
    vec3 s;
    
    //roughness (modify the surface normal)
    vec3 normal = Ray.norm  + randv((bounce+1)*seed)*Ray.distO.surf.roughness*0.99;
    normal = normal.normalize();
    
    
    //generate new direction, depends on material
    switch(rayType){
        case 1:
            //reflections:
            this->dir = (this->dir).rotate(normal, PI);
            this->dir = this->dir * -1;
            
//                 this->dir = this->dir + randv(bounce*seed)*Ray.distO.surf.roughness;
            
            break;
        case 2:
            //this is for refractions:
            //are we coming out or going into the surface?
            sig = (1-2*(this->dir.dot(normal)<0));
            
            //get vector at right angles, depending on outgoing or incoming
            s = this->dir.cross(normal*sig);
            //calculate the refraction-angle (angle between new-ray and normal)
            tnew = s.abs()*(1/Ray.distO.surf.ior)*(sig<0) + s.abs()*(Ray.distO.surf.ior)*(sig>0);
            
            //the refracted ray's direction is a rotated normal (about the orthogonal s)
            this->dir = normal.rotate(s,-asin(tnew)) * sig;
            
//                 this->dir = this->dir + randv(bounce*seed)*Ray.distO.surf.roughness;
            
            break;
        default:
            //todo: make diffuse eventually
            this->dir = (this->dir).rotate(Ray.norm, PI);
            this->dir = this->dir * -1;
            
//                 this->dir = this->dir + randv(bounce*seed)*Ray.distO.surf.roughness;
            
            break;
    }
    
    
    
    //step away, so as to no sit on the surface and be unable to move (as dist == 0)
    //this should use a constant, but the constant is undefined in device-code
    this->pos = this->pos + (this->dir* 0.01);
}



//--------------------------------------------------------------//
//      helpfull stuff
//--------------------------------------------------------------//


__device__ __host__
vec3 sky(vec3 V){
    return vec3(1,1,1);
    //this is a simple sky.
    //TODO this function is currently disabled
    vec3 Pixel;
    double tel = 1-V.sphC().y/4/PI;
    Pixel.x *= tel*0.8;
    Pixel.y *= tel*tel*0.9;
    Pixel.z *= tel*tel*tel;
    return V.sphC();
}


__device__ __host__
double randd(int seed){
    double f;
    f = (double) seed / (seed % 47 + 1);
    f += (double) seed / (seed % 149 + 1);
    f = (double) seed / (seed % 19 + 1);
    f += (double) seed / (seed % 3461 + 1);
//     while(f>1||f<-1)
//         f = f/seed;
    return f;
}

__device__ __host__
vec3 randv(int seed){
    vec3 ret;
    ret.x = randd(seed*2+73);
    ret.y = randd(seed*3+71);
    ret.z = randd(seed+83);
    return ret.normalize();
}

__device__ __host__
double dmod2(double n, int div){
    while(n<0)
        n+=div;
    while(n>div)
        n-=div;
    return n;
}
