#include "output.h"



output::output(){
    this->i=1;
}

output::output(const char fileName[]){
    //this->fileName[] = fileName;
    this->i=2;
}

void output::writeRgba1(const char fileName[], const Rgba *pixels, int width, int height){
    
    RgbaOutputFile file (fileName, width, height, WRITE_RGBA);
    file.setFrameBuffer (pixels, 1, width);                         
    file.writePixels (height);                                      
}

//output& output::operator=(const output&){}



void output::save(std::vector<pixel> *image, const char fileName[], int xRes, int yRes){
    int pixCount = xRes*yRes;
    Rgba *pixs;
    pixs = (Rgba*) malloc(pixCount*sizeof(Rgba));
    for(int i = 0; i<pixCount; i++){
            //cout<<"t127"<<endl;

        Rgba pixel;
        pixel.r = ((*image)[i].diff_col.x * (*image)[i].diff_lig.x + (*image)[i].glos_col.x * (*image)[i].glos_lig.x + (*image)[i].tran_col.x * (*image)[i].tran_lig.x);
        pixel.g = ((*image)[i].diff_col.y * (*image)[i].diff_lig.y + (*image)[i].glos_col.y * (*image)[i].glos_lig.y + (*image)[i].tran_col.y * (*image)[i].tran_lig.y);
        pixel.b = ((*image)[i].diff_col.z * (*image)[i].diff_lig.z + (*image)[i].glos_col.z * (*image)[i].glos_lig.z + (*image)[i].tran_col.z * (*image)[i].tran_lig.z);
        
        
        
        //different debuggin options:
//         pixel.r = (*image)[i].diff_lig.x;
//         pixel.g = (*image)[i].glos_lig.y;
//         pixel.b = (*image)[i].tran_lig.z;
        
//         pixel.r = (*image)[i].diff_lig.x-(*image)[i].diff_col.x;
//         pixel.g = (*image)[i].glos_lig.y-(*image)[i].glos_col.x;
//         pixel.b = (*image)[i].tran_lig.z-(*image)[i].tran_col.x;
        
//         pixel.r = (*image)[i].diff_col.x;
//         pixel.g = (*image)[i].glos_col.x;
//         pixel.b = (*image)[i].tran_col.x;
        
        
        
        pixel.a = (*image)[i].alpha;
//         pixel.a = 1;
//         pixel.r = pixel.g = pixel.b = (*image)[i].distance;
        pixs[i]=pixel;
    }
    this->writeRgba1(fileName,pixs,xRes,yRes);
}
