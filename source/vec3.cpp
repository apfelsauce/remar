#include "vec3.h"


__device__ __host__
vec3::vec3(double x, double y, double z){
    this->x = x;
    this->y = y;
    this->z = z;
}


//------------------operator overloads---------------------//        
__device__ __host__
vec3 vec3::operator+(const vec3& v){
    vec3 vec(0,0,0);
    vec.x = this->x + v.x;
    vec.y = this->y + v.y;
    vec.z = this->z + v.z;
    return vec;
}

__device__ __host__
vec3 vec3::operator-(const vec3& v){
    vec3 vec(0,0,0);
    vec.x = this->x - v.x;
    vec.y = this->y - v.y;
    vec.z = this->z - v.z;
    return vec;
}

//two operator overloads for scaling with ints(one alone doesn't seem to do the trick.)
__device__ __host__
vec3 vec3::operator*(const int& i){
    vec3 vec(0,0,0);
    vec.x = this->x *i;
    vec.y = this->y *i;
    vec.z = this->z *i;
    return vec;
}



//two operator overloads for scaling with doubles(one alone doesn't seem to do the trick.)
__device__ __host__
vec3 vec3::operator*(const double& i){
    vec3 vec(0,0,0);
    vec.x = this->x *i;
    vec.y = this->y *i;
    vec.z = this->z *i;
    return vec;
}



//---------------end of operator overloads------------------//


//#########################################################//
//--------this is the beginning of normal methods----------//

//dot-product:
__device__ __host__
double vec3::dot(vec3 v){
    double dtx = this->x * v.x;
    double dty = this->y * v.y;
    double dtz = this->z * v.z;
    return dtx+dty+dtz;
}
        
//cross-product:
__device__ __host__
vec3 vec3::cross(vec3 v){
    vec3 res;
    res.x = y*v.z-z*v.y;
    res.y = z*v.x-x*v.z;
    res.z = x*v.y-y*v.x;
    
    return res;
}

//weird scale:
__device__ __host__
vec3 vec3::vScale(vec3 v){
    return vec3(this->x*v.x,this->y*v.y,this->z*v.z);
}

//absolute value (length)
__device__ __host__
double vec3::abs(){
    return sqrt(this->x*this->x+this->y*this->y+this->z*this->z);
}

__device__ __host__
vec3 vec3::pos(){
    vec3 a;
    a.x = (0 < this->x)*this->x - (this->x < 0)*this->x;
    a.y = (0 < this->y)*this->y - (this->y < 0)*this->y;
    a.z = (0 < this->z)*this->z - (this->z < 0)*this->z;
    return a;
}

//normalized version of the vector
__device__ __host__
vec3 vec3::normalize(){
    vec3 a;
    double ass = this->abs();
    
    a.x = this->x / ass;
    a.y = this->y / ass;
    a.z = this->z / ass;
    
    return a;
}


//Stuff for rotation, axis-specific could be removed?

//shady rotation matrix buisness going on in here!
__device__ __host__
vec3 vec3::rotateX(double angle){
    vec3 v;
    v.x = this->x;
    v.y = this->y * cos(angle) - this->z * sin(angle);
    v.z = this->z * cos(angle) + this->y * sin(angle);
    return v;
}

__device__ __host__
vec3 vec3::rotateY(double angle){
    vec3 v;
    v.x = this->x * cos(angle) + this->z * sin(angle);
    v.y = this->y;
    v.z = this->z * cos(angle) - this->x * sin(angle);
    return v;
}

__device__ __host__
vec3 vec3::rotateZ(double angle){
    vec3 v;
    v.x = this->x * cos(angle) - this->y * sin(angle);
    v.y = this->y * cos(angle) + this->x * sin(angle);
    v.z = this->z;
    return v;
}

__device__ __host__
vec3 vec3::rotate(vec3 u, double angle/*in radians*/){
    vec3 v;
    u = u.normalize();
    
    //the "rotation quarternion"
    double s = sin(angle/2);
    quart q(cos(angle/2),u.x*s,u.y*s,u.z*s);
    quart p(0, this->x, this->y, this->z);
    p = q*p*q.conjugate();
    v.x = p.i;
    v.y = p.j;
    v.z = p.k;
    return v;
}
//this is the old rotation-matrix code

// //even worse stuff, straight from wikipedia!
// __device__ __host__
// vec3 vec3::rotate(vec3 u, double angle/*in radians*/){
//     u = u.normalize();
//     vec3 v;
//     //this is going to be bad!
//     v.x = this->x*(cos(angle)+u.x*u.x*(1-cos(angle))) +
//             this->y*(u.x*u.y*(1-cos(angle))-u.z*sin(angle)) +
//             this->z*(u.x*u.z*(1-cos(angle))-u.y*sin(angle));
//     
//     v.y = this->x*(u.x*u.y*(1-cos(angle))-u.z*sin(angle)) +
//             this->y*(cos(angle)+u.y*u.y*(1-cos(angle))) +
//             this->z*(u.y*u.z*(1-cos(angle))-u.x*sin(angle));
//     
//     v.z = this->x*(u.z*u.x*(1-cos(angle))-u.y*sin(angle)) +
//             this->y*(u.z*u.y*(1-cos(angle))-u.x*sin(angle)) +
//             this->z*(cos(angle)+u.z*u.z*(1-cos(angle)));
//     
//     return v;
// }


vec3 vec3::sphC(){
    vec3 v;
    double r = this->abs();
    v = this->normalize();
    double f;
    if(v.x!=0)
        f = atan(v.y/v.x);
    else{
        if(v.y<0)
            f = 3*PI/2;
        else if(v.y == 0)
            f = 0;
        else
            f = PI/2;
    }
    double t = acos(v.z/r);
    return vec3(r,t,f);
}
