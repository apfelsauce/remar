#ifndef _SCENE
#define _SCENE

#include <iostream>
#include <vector>
#include <string>
#include <cstring>
//#include <boost/format.hpp>

#include "vec3.h"
#include "camera.h"
#include "de_object.h"
#include "frame.h"


//using namespace std;

class scene{
public:
    int xRes;
    int yRes;
    camera Cam;
    output Output;
    
    int samples = 3;
    
    
    
    scene(int x,int y,output Out);
    
    void render(frame Frame,const char * filename);
    
    void renderAnim(std::vector<frame> frames, const char * filename);
};


#endif
