#include "de_object.h"

//this is where specific things would have to be changed, depending on the object to be rendered.

__device__ __host__
double dmod(double n, int div);

__device__ __host__
de de_object::DE(vec3 pos){
    de Dist;
    pos.x = dmod(pos.x, 3)-1.5;
    pos.y = dmod(pos.y, 3)-1.5;
    if(pos.z<0)
        pos.z +=3;
    pos.z -=1;
    //pos.z = dmod(pos.z, 3)-1.5;
    Dist.distance = pos.abs() -1;
    if (Dist.distance < 0)
        Dist.distance *= -1;
    Dist.norm = pos.normalize();
    
//     Dist.surf = surfer(vec3(0.98,0.98,0.98),vec3(0,0,0),vec3(0,0,0),0.1,1.33);
//     Dist.surf = surfer(vec3(0.98,0.98,0.98),vec3(0.5,0.4,0.3),vec3(0,0,0),0.1,1.33);
//     Dist.surf = surfer(vec3(0,0,0),vec3(1,0.2,0.1),vec3(0.8,0.85,0.9),0.1,1.45);
//     Dist.surf = surfer(vec3(0,0,0),vec3(0.8,0.8,0.81),vec3(0,0,0),0.3,1.45);
    Dist.surf = surfer(vec3(0,0,0),vec3(0,0,0),vec3(0.8,0.8,0.81),0.3,1.45);
    
    return Dist;
}

void de_object::update(std::vector<double> params){
    //add more code, maybe an array of doubles internally?
    int a = 1;
};

__device__ __host__
double dmod(double n, int div){
    while(n<0)
        n+=div;
    while(n>div)
        n-=div;
    return n;
}
