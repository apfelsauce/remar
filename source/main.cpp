//This is the main cpp file.
//
//compilation note: g++ main.cpp
//


#include <iostream>
#include <cmath>
#include <cstdlib>
#include <vector>
#include <fstream>



#include "vec3.h"
#include "pixel.h"
#include "output.h"
#include "de_object.h"
#include "ray.h"
#include "sample.h"
#include "camera.h"
#include "scene.h"
#include "frame.h"
#include "io.h"



using namespace std;

int main(){
    
    
    ioUtil ioHandler("test.rem",1);
    
    std::vector<frame> Frames;
    Frames = (ioHandler.read_file("test.rem"));
    
    
    scene mainScene(500,500,output("filename1"));
    
    output Out("filename1");
    
    frame Frame = Frames[0];
    // Frame.camPos = vec3(0,0,5);
    // Frame.camDir = vec3(0,1,-.4);
    // Frame.camUp = vec3(0,0,1);
    // Frame.fLen = 28.6;
//     
//     mainScene.render(Frame,"filename.exr");
    
//     std::vector<frame> Frames2;
//     Frames2.push_back(Frame);
//     ioHandler.write_file(Frames2,"test.rem2");
    
    mainScene.render(Frame,"filename.exr");
    
    return 0;
}
