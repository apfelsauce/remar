#include "io.h"


frame ioUtil::parseLine(string line){
    frame parsed;
    char Line[line.size()+1];
    strcpy(Line, line.c_str());
    
    char t[20];
    char a = Line[0];
    int i = 0;
    int j = 0;
    int p = 0;
    
    int f=1;
    while(f){
        a = Line[i];
        i++;
        //if a comma is reached, stop and parse
        if(a==','||a==0){
            t[j] = 0;
            //cout<<t<<endl;
            j = 0;
        //parse
            switch(p){
                case 0:
                    parsed.camPos.x = atof(t);
                    break;
                case 1:
                    parsed.camPos.y = atof(t);
                    break;
                case 2:
                    parsed.camPos.z = atof(t);
                    break;
                case 3:
                    parsed.camDir.x = atof(t);
                    break;
                case 4:
                    parsed.camDir.y = atof(t);
                    break;
                case 5:
                    parsed.camDir.z = atof(t);
                    break;
                case 6:
                    parsed.camUp.x = atof(t);
                    break;
                case 7:
                    parsed.camUp.y = atof(t);
                    break;
                case 8:
                    parsed.camUp.z = atof(t);
                    break;
                case 9:
                    parsed.fLen = (float) atof(t);
                    break;
                default:
                    parsed.params.push_back(atof(t));
                    break;
            }
            p++;
            if(a==0)
                f=0;
        }else{
            t[j] = a;
            j++;
        }
    }
    //printframe(parsed);
    return parsed;
}
    
ioUtil::ioUtil(const char fileName[], int n){
    for(int i = 0; i<n; i++)
        (this->file_name).push_back(fileName[i]);
}


int ioUtil::write_file(std::vector<frame> frames, const char fileName[]){
    ofstream wf;
    wf.open(fileName);
    if(! wf)
        return 1;
    int s = frames.size();
    //wf<<s<<endl;
    for(int i=0; i<s;i++){
        frame curr=frames[i];
        wf<<curr.camPos.x<<","<<curr.camPos.y<<","<<curr.camPos.z<<",";
        wf<<curr.camDir.x<<","<<curr.camDir.y<<","<<curr.camDir.z<<",";
        wf<<curr.camUp.x <<","<<curr.camUp.y <<","<<curr.camUp.z <<",";
        wf<<curr.fLen;
        for(int e=0; e <curr.params.size(); e++)
            wf<<","<<curr.params[e];
        wf<<endl;
    }
    if(! wf.good())
        return -1;
    return 0;
}

int ioUtil::write_file(std::vector<frame> frames){
    return write_file(frames, &this->file_name[0]);
}


std::vector<frame> ioUtil::read_file(const char fileName[]){
    std::vector<frame> frames;
    
    ifstream rf;
    rf.open(fileName);
    
    if(! rf){
        cout<<"IO-Error: could not read file "<<fileName<<"!"<<endl;
        return frames;
    }
    //int s;
    //rf.read((char *) &s, sizeof(int));
    //frames.resize(s);
    string line;
    while(getline(rf,line)){
        frame F = parseLine(line);
        printframe(F);
        frames.push_back(F);
    }
    
    if(! rf.good()){
        cout<<"IO-Error: Error while reading file "<<fileName<<" (butlessbad)!"<<endl;
        return frames;
    }
    return frames;
}

std::vector<frame> ioUtil::read_file(){
    return read_file(&this->file_name[0]);
}
