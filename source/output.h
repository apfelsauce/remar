#ifndef _OUTPUT
#define _OUTPUT

#include <iostream>
#include <vector>

#include <OpenEXR/ImfOutputFile.h>
#include <OpenEXR/ImfChannelList.h>
#include <OpenEXR/ImfArray.h>
#include <OpenEXR/ImfCompression.h>
#include <OpenEXR/ImfStringAttribute.h>
#include <OpenEXR/ImfIntAttribute.h>
#include <OpenEXR/ImfBoxAttribute.h>
#include <OpenEXR/ImfDoubleAttribute.h>
#include <OpenEXR/ImfStringVectorAttribute.h>
#include <OpenEXR/ImfTimeCodeAttribute.h>
#include <OpenEXR/ImfStandardAttributes.h>
#include <OpenEXR/ImfFramesPerSecond.h>
#include <OpenEXR/ImfMatrixAttribute.h>
#include <OpenEXR/ImathBox.h>
#include <OpenEXR/half.h>
#include <OpenEXR/ImfRgba.h>
#include <OpenEXR/ImfRgbaFile.h>

#include "pixel.h"

using namespace std;
using namespace Imf_2_2;


class output{
public:
    int i = 0;
    
    //char fileName[];
    
    output();
    output(const char fileName[]);
    
    //output& output::operator=(const output&){}
    
    void writeRgba1(const char fileName[], const Rgba *pixels, int width, int height);
    
    
    
    void save(std::vector<pixel> *image, const char fileName[], int xRes, int yRes);
    
};

#endif
