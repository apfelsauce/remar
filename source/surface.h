#ifndef _SURF
#define _SURF

#ifndef __device__
#define __device__
#endif

#ifndef __host__
#define __host__
#endif

#include <iostream>

#include "vec3.h"


class surfer{
public:
    vec3 diff_col;
    vec3 glos_col;
    vec3 tran_col;
    
    float roughness;
    float ior;
    
    __device__ __host__
    surfer(){}
    
    __device__ __host__
    surfer(vec3 dif, vec3 gls, vec3 trs, float rgh, float indexOfRefraction);
    
    __device__ __host__
    int surfType(int rand);
};

#endif
