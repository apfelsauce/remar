//This is the main cu file.
//
//compilation note: nvcc main.cu
//
//This file is currently not used.

#include <iostream>
#include <cmath>
#include <cstdlib>
#include <vector>
#include <fstream>



#include "vec3.h"
#include "pixel.h"
#include "output.h"
#include "de_object.h"
#include "ray.h"
#include "sample.h"
#include "camera.h"
#include "scene.h"



using namespace std;

int main(){
    output Out("filename");
    
    scene mainScene(1920,1080,output("filename"));
    
    frame Frame;
    
    mainScene.render(Frame"filename");
    
    return 0;
}
