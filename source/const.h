#ifndef _CONST
#define _CONST

#ifndef PI
#define PI 3.14159265
#endif

#ifndef INFT
#define INFT 1000
#endif

#ifndef MAXSTEP
#define MAXSTEP 512
#endif

#ifndef MAXLEN
#define MAXLEN 30
#endif

//distance at which it will prematurely terminate
#ifndef MINDIST
#define MINDIST
const double mindist = 0.0001;
#endif

//if all the steps are used, this is the minimum distance required to score a hit
#ifndef MINHIT
#define MINHIT
const double minhit = 0.01;
#endif

#endif
