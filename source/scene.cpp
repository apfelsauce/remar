#include "scene.h"


scene::scene(int x,int y,output Out){
    this->xRes = x;
    this->yRes = y;
    this->Output = Out;
}

void scene::render(frame Frame,const char * filename){
    vec3 camPos = Frame.camPos;
    vec3 camDir = Frame.camDir;
    vec3 camUp  = Frame.camUp;
    de_object DE;
    DE.update(Frame.params);
    this->Cam = camera(Frame.fLen,this->xRes,this->yRes,DE,this->samples,camPos,camDir,camUp);
    std::vector<pixel> image;
    Cam.render(&image);
    Output.save(&image,filename,this->xRes,this->yRes);
}

void scene::renderAnim(std::vector<frame> Frames, const char * filename){
    int len = Frames.size();
    for(int i = 0; i<len; i++){
        // replaced boost with c++
        //boost::format fmter = boost::format("%3d-%s") % i % filename;
        //string fName = fmter.str();
        
        //determine filename with framenumber (4-digits)
        string s(filename);
        string fName = std::to_string(i) + "-" + s;
        char fNam[fName.size() + 1];
        strcpy(fNam, fName.c_str());
        this->render(Frames[i], fNam);
    }
}
