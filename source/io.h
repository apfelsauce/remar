#ifndef _RIO
#define _RIO

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cstring>

#include "frame.h"


class ioUtil{
    
public:
    std::vector<char> file_name;
    
    ioUtil(const char fileName[], int n);
    
    int write_file(std::vector<frame> frames, const char fileName[]);
    
    int write_file(std::vector<frame> frames);
    
    std::vector<frame> read_file(const char fileName[]);
    
    std::vector<frame> read_file();
    
    frame parseLine(string line);
};

#endif
