#ifndef _FRAME
#define _FRAME

#include <vector>

#include "vec3.h"



struct frame{
    vec3 camPos;
    vec3 camDir;
    vec3 camUp;
    float fLen;
    std::vector<double> params;
};

void printframe(frame F);

#endif
