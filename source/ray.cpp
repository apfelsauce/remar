#include "ray.h"

__device__ __host__
ray::ray(vec3 position, vec3 direction, de_object DEin){
    this->dir = direction;
    this->pos = position;
    this->DE = DEin;
}

__device__ __host__
int ray::march_step(){
    this->distO = this->DE.DE(this->pos);
    double dist = this->distO.distance;
    //the new position is marched by distance but also repositioned by the DE
    this->pos = this->distO.repos + (this->dir * dist);
    if(dist < mindist)
        return 3;
    if(dist < minhit)
        return 2;
    if(dist > MAXLEN)
        return -1;
    return 0;
}

__device__ __host__
bool ray::march_to_end(){
    int step = 0;
    int ret = 0;
    while(step<MAXSTEP && ret<3 && ret>=0){
        ret = this->march_step();
        step++;
    }
    this->norm = this->distO.norm;
    if(ret > 1)
        return true;
    return false;
}
