#include "de_object.h"



//this is where specific things would have to be changed, depending on the object to be rendered.



__device__ __host__
double dmod(double n, int div);

__device__ __host__
double sierpinski(double f){
    f = f*6;
    if(abs(f)<2){
        f += -4+8*signbit(f);
    }
    f=f/6;
    return f;
}


__device__ __host__
de de_object::DE(vec3 p){
    de Dist;
    Dist.repos = p;
    double scale = 1;
    // p.x = dmod(p.x, 3)-1.5;
    // p.y = dmod(p.y, 3)-1.5;
    if(abs(p.x)<1/3 && abs(p.y)<1/3 && abs(p.z)<1/3){
        scale = 6;
    }
    
    
    
    p.x = sierpinski(p.x);
    p.y = sierpinski(p.y);
    p.z = sierpinski(p.z);
    
    
    
    vec3 pt = p;
    pt = pt.pos();
    
    vec3 box = vec3(0.5,0.5,0.5);
    
    vec3 t = pt-box;
    vec3 td(max(t.x,0.0), max(t.y,0.0), max(t.z,0.0));
    
    Dist.distance = td.abs()/scale;
    
    Dist.norm = vec3(td.x*((0 < p.x) - (p.x < 0)), td.y*((0 < p.y) - (p.y < 0)), td.z*((0 < p.z) - (p.z < 0))).normalize();
    
    //cout<<Dist.norm<<endl;
    
    Dist.surf = surfer(vec3(dmod(p.x,1),dmod(p.y,1),dmod(p.z,1)),vec3(0.5,0.4,0.3),vec3(0,0,0),0.1,1.33);
    
    return Dist;
}

// __device__ __host__
// de de_object::DE(vec3 p) {
//     vec3 z = p;
//     de Dist;
//     int Scale = 2;
//     int n= 0;
//     vec3 a1 = vec3(1,1,1);
//     vec3 a2 = vec3(-1,-1,1);
//     vec3 a3 = vec3(1,-1,-1);
//     vec3 a4 = vec3(-1,1,-1);
//     vec3 c;
//     double dist, d;
//     while(n<3){
//         c = a1; dist = (z-a1).abs();
//         d = (z-a2).abs(); if (d < dist){
//             c = a2;
//             dist=d;
//         }
//         d = (z-a3).abs(); if (d < dist){
//             c = a3;
//             dist=d;
//         }
//         d = (z-a4).abs(); if (d < dist){
//             c = a4;
//             dist=d;
//         }
//         cout<<"ca"<<n<<endl;
//         z = (Scale*z)-(c*(Scale-1.0));
//         cout<<"cf"<<n<<endl;
//         n +=1;
//         cout<<"c"<<n<<endl;
//     }
//     cout<<"a"<<endl;
//     Dist.distance = (z.abs()-0.5)*pow(Scale, float(-n));
// //     Dist.distance = dist;
//     double EPS = 0.01;
//     
//     Dist.norm = z;
//     Dist.surf = surfer(vec3(0.9,0.9,0.9),vec3(0.5,0.4,0.3),0.1);
//     
//     return Dist;
// }

void de_object::update(std::vector<double> params){
    //add more code, maybe an array of doubles internally?
    int a = 1;
};





    

__device__ __host__
double dmod(double n, int div){
    while(n<0)
        n+=div;
    while(n>div)
        n-=div;
    return n;
}
