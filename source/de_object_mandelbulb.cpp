#include "de_object.h"



//this is where specific things would have to be changed, depending on the object to be rendered.


__device__ __host__
double mand(quart p, int its){
    quart z = quart(0,0,0,0);
    for(int i =0; i<its;i++){
    z = (z*z)+p;
    }
    return z.abs();
}


__device__ __host__
de de_object::DE(vec3 p) {
    double EPS = 0.01;
    int its = 10;
    de Dist;
    quart Q= quart(p.x,p.y,p.z,0);
    float r = mand(Q,its);
    quart grad = quart(mand(Q+quart(1,0,0,0)*EPS,its),mand(Q+quart(0,1,0,0)*EPS,its),mand(Q+quart(0,0,1,0)*EPS,its),0)*(1/EPS);
    Dist.distance = 0.5*log(r)*r/grad.abs();
    Dist.norm = vec3(grad.e,grad.i,grad.j);
    Dist.surf = surfer(vec3(0.9,0.9,0.9),vec3(0.5,0.4,0.3),0.1);
    
    return Dist;
}

void de_object::update(std::vector<double> params){
    //add more code, maybe an array of doubles internally?
    int a = 1;
};

